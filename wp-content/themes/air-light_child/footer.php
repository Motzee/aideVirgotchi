<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package air-light
 */

?>

	</div><!-- #content -->

	<footer role="contentinfo" id="colophon" class="site-footer">

            <p><a href="http://localhost:8777" />Revenir au jeu Virgotchi</a></p>

	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
