��    ?        Y         p     q  
   y     �  
   �  
   �     �     �     �     �     �     �  
          h   *     �  8   �     �     �          %     ?     P     `     q          �     �     �     �     �  
   �     �     �  	   �  	   �     �  
   	               $     6     F     O     d     v     �     �     �     �     �     �     �     �  
   	     	     	     $	     >	     O	  	   V	     `	     f	    o	     �
     �
  '   �
     �
     �
     �
     �
       
     "         ;     \     j  |   �     �  P        e      �     �     �     �     �          "     3     <     J     b     v     z     �     �     �  	   �     �     �     �  	   �               %  	   9  #   C     g      �     �     �     �     �     �               1     ?     N     R     a     {     �  	   �     �     �        9       &   (              4         
   )       5                                                      ,                        :   2      8   	      -          ;   %   1          >              <          #          /   +   *                           0            '          6          .   ?   7   $   3   =   "   !    %s hour Above code Activate Pastacode for comments Back to %s Below code By %s Caching duration Code Code ID Code description location Code editor settings Code title Editor appareance Embed GitHub, Gist, Pastebin, Bitbucket or whatever remote files and even your own code by copy/pasting. Expand editor Experimental mode, can slow down website on front-end... File from uploads File path inside the repository File path relative to %s Filename (with extension) General Settings Highlited lines Import code (%s) Insert a code John Doe Lines: Manual Code Editor Never reload No No cache (dev mode) Once Daily Once Weekly Past'a code Pastacode Provider: Purge cache Repository Revision See %s Select a provider Select a syntax Settings Show invisible chars Show line numbers Show preview on editor Syntax Coloration Style Syntax: Title: Twice Daily User of repository Visibles lines Visit author homepage Willy Bahuaud Write code Yes bin/foobar.php http://pastacode.wabeo.fr https://wabeo.fr master pastacode title view raw PO-Revision-Date: 2016-12-20 08:10:55+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/2.4.0-alpha
Language: fr
Project-Id-Version: Plugins - Pastacode - Stable (latest release)
 %s hour Au dessus du code Activer Pastacode pour les commentaires Retour à %s En dessous du code Par %s Durée du cache Code ID du code Position de la description du code Réglage de l’éditeur de code Titre du code Thème de l’éditeur Intégrer GitHub, Gist, Pastebin, Bitbucket ou vos fichiers à distance et même votre propre code par simple copier/coller. Étendre l’éditeur Fonctionnalité expérimentale, peut légèrement freiner le chargement du site. Fichier dans la bibliothèque Chemin du fichier dans le dépot Chemin du fichier relatif à %s Nom du fichier (avec extension) Réglages généraux Lignes soulignées Importer du code (%s) Insérer un code John Doe Lignes&nbsp;: Édition de code manuel Ne jamais recharger Non Pas de cache (mode dev) Une fois par jour Une fois par semaine Past'a code Pastacode Fournisseur&nbsp;: Vider le cache Dépot Révision Afficher %s Choisir un fournisseur Choisir une syntaxe Réglages Afficher les caractères invisibles Afficher les numéros de ligne Prévisualiser dans l’éditeur Thème de coloration Syntaxe&nbsp;: Titre&nbsp;: Deux fois par jour Propriétaire du dépot Lignes visibles Visitez le site de l’auteur Willy Bahuaud Saisir du code Oui bin/foobar.php http://pastacode.wabeo.fr https://wabeo.fr master pastacode Titre affichage brut 